// const http = require('http');
// const express = require('express');
// const path = require('path');
// const app = express();

import http from 'http';
import express from 'express';
import path from 'path';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));


const app = express();
app.use(express.json());
app.use(express.static("express"));
app.use('/', function(req,res){
    console.log(__dirname);
    res.sendFile(path.join(__dirname + '/express/index.html'));
    //__dirname : It will resolve to your project folder.
  });

const server = http.createServer(app);
const port = 3333;
server.listen(port);
console.debug('Server listening on port ' + port);
