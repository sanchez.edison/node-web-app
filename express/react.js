'use strict'; 

// // import { Button } from 'esc-components';
// const e = React.createElement; 

// // class myButton extends React.Component { 
// //   constructor(props) { 
// //     super(props); 
// //     this.state = { isliked: false }; 
// //   } 
 
// //   render() { 
// //     if (this.state.isliked) { 
// //       return 'Yes I Really Like This.'; 
// //     } 
 
// //     return e( 
// //       'button', 
// //       { onClick: () => this.setState({ isliked: true }) }, 
// //       'Like Button' 
// //     ); 
// //   } 
// // } 
// const domContainer = document.querySelector('esc_container'); 
// ReactDOM.render(e(myButton), domContainer);

const e = React.createElement;




// Display a "Like" <button>
ReactDOM.render(e(
  'button',
  { onClick: () => this.setState({ liked: true }) },
  'Like'
), document.getElementById('esc_container'));

